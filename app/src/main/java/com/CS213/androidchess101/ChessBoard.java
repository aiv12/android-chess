package chess;

import chesspieces.Bishop;
import chesspieces.King;
import chesspieces.Knight;
import chesspieces.Pawn;
import chesspieces.Piece;
import chesspieces.Queen;
import chesspieces.Rook;

/**
 * Class that creates a static instance of a Chessboard that can be accessed by all classes.
 * The Chess board is a 2D Array of type ChessTile
 * 
 * @author David Acevedo
 * @author Alex Labrunda
 */

public class ChessBoard {
	
	/**
	 * Board that keeps track of all pieces
	 */
	private static ChessTile[][] board;
	
	/**
	 * Boolean that keeps track of whose turn it is
	 */
	private static boolean isWhiteMove;
	
	/**
	 * Keeps track of the previous move made
	 */
	private String previousMove;
	
	/**
	 * Tracks the last piece moved (Mainly for En Passant)
	 */
	private static Piece lastPieceMoved;
	
	/**
	 * String containing valid ranks
	 */
	private final String validRank = "abcdefgh";
	
	/**
	 * String containing all valid files
	 */
	private final String validFile = "12345678";
	
	/**
	 * String thqt contains valid pieces to promote
	 */
	private final String validPromotion = "NBRQ";
	
	/**
	 * Constructor that initiates:
	 * <ul>
	 * 	<li>The playing board.</li>
	 * 	<li>All black pieces on the playing board.</li>
	 * 	<li>All white pieces on the playing board.</li>
	 * </ul>
	 */
	public ChessBoard() {
		
		//Initiate the board
		board = new ChessTile[8][8];
		
		//Initiate all black pieces on the board
		board[0][0] = new ChessTile(0, 0, new Rook(0, 0, "White"));
		board[1][0] = new ChessTile(1, 0, new Knight(1, 0, "White"));
		board[2][0] = new ChessTile(2, 0, new Bishop(2, 0, "White"));
		board[3][0] = new ChessTile(3, 0, new Queen(3, 0, "White"));
		board[4][0] = new ChessTile(4, 0, new King(4, 0, "White"));
		board[5][0] = new ChessTile(5, 0, new Bishop(5, 0, "White"));
		board[6][0] = new ChessTile(6, 0, new Knight(6, 0, "White"));
		board[7][0] = new ChessTile(7, 0, new Rook(7, 0, "White"));
		board[0][1] = new ChessTile(0, 1, new Pawn(0, 1, "White"));
		board[1][1] = new ChessTile(1, 1, new Pawn(1, 1, "White"));
		board[2][1] = new ChessTile(2, 1, new Pawn(2, 1, "White"));
		board[3][1] = new ChessTile(3, 1, new Pawn(3, 1, "White"));
		board[4][1] = new ChessTile(4, 1, new Pawn(4, 1, "White"));
		board[5][1] = new ChessTile(5, 1, new Pawn(5, 1, "White"));
		board[6][1] = new ChessTile(6, 1, new Pawn(6, 1, "White"));
		board[7][1] = new ChessTile(7, 1, new Pawn(7, 1, "White"));
		
		//Initiates all white pieces
		board[0][6] = new ChessTile(0, 6, new Pawn(0, 6, "Black"));
		board[1][6] = new ChessTile(1, 6, new Pawn(1, 6, "Black"));
		board[2][6] = new ChessTile(2, 6, new Pawn(2, 6, "Black"));
		board[3][6] = new ChessTile(3, 6, new Pawn(3, 6, "Black"));
		board[4][6] = new ChessTile(4, 6, new Pawn(4, 6, "Black"));
		board[5][6] = new ChessTile(5, 6, new Pawn(5, 6, "Black"));
		board[6][6] = new ChessTile(6, 6, new Pawn(6, 6, "Black"));
		board[7][6] = new ChessTile(7, 6, new Pawn(7, 6, "Black"));
		board[0][7] = new ChessTile(0, 7, new Rook(0, 7, "Black"));
		board[1][7] = new ChessTile(1, 7, new Knight(1, 7, "Black"));
		board[2][7] = new ChessTile(2, 7, new Bishop(2, 7, "Black"));
		board[3][7] = new ChessTile(3, 7, new Queen(3, 7, "Black"));
		board[4][7] = new ChessTile(4, 7, new King(4, 7, "Black"));
		board[5][7] = new ChessTile(5, 7, new Bishop(5, 7, "Black"));
		board[6][7] = new ChessTile(6, 7, new Knight(6, 7, "Black"));
		board[7][7] = new ChessTile(7, 7, new Rook(7, 7, "Black"));
		
		//Initializes the rest of the board
		for(int i = 0; i < 8; i++) {
			for(int j = 2; j < 6; j++) {
				board[i][j] = new ChessTile(i, j);
			}
		}
		
		isWhiteMove = true;
		
	}
	
	/**
	 * Method to make the user-specified move
	 * @param move the user-specified move
	 * @param colorTurn the color of the person's turn
	 * @return GameState enum that provides information about the current state of the game
	 */
	GameState makeMove(String move, String colorTurn) {
		move = move.trim();
		if (!isValidMove(move)) {
			return GameState.INVALID_MOVE;
		}
		if (previousMove != null && previousMove.contains("draw?") && move.equals("draw")) {
			return GameState.DRAW;
		}
		if (move.equalsIgnoreCase("resign")) {
			if (isWhiteMove) {
				return GameState.WHITE_RESIGN;
			}
			else {
				return GameState.BLACK_RESIGN;
			}
		}
		return movePiece(move);
	}
	
	/**
	 * Method to determine whether a user-specified move is valid
	 * @param move the user-specified move
	 * @return true if the move is valid, false otherwise
	 */
	private boolean isValidMove(String move) {
		if (previousMove != null && previousMove.contains("draw?") && move.contains("draw?")) {
			return false;
		}
		if (previousMove != null && previousMove.contains("draw?") && move.equals("draw")) {
			return true;
		}
		if (move.equals("resign")) {
			return true;
		}
		String[] coordinates = move.split(" ");
		if (coordinates.length != 2 && coordinates.length != 3) {
			return false;
		}
		String originSpace = coordinates[0];
		if (!isValidCoordinate(originSpace)) {
			return false;
		}
		String destinationSpace = coordinates[1];
		if (!isValidCoordinate(destinationSpace)) {
			return false;
		}
		
		int originFile = originSpace.charAt(0) - 97;
		int originRank = originSpace.charAt(1) - 49;
		
		Piece piece = board[originFile][originRank].getPiece();
		
		int destinationFile = destinationSpace.charAt(0) - 97;
		int destinationRank = destinationSpace.charAt(1) - 49;
		
		if(isWhiteMove && piece.isBlack()) {
			return false;
		}
		
		if(!isWhiteMove && piece.isWhite()) {
			return false;
		}
		
		if (!piece.isAllowableMove(destinationFile, destinationRank)) {
			return false;
		}
		
		if (isInvalidPromotion(piece, move)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to determine whether a user-specified coordinate (File, Rank) is valid
	 * @param fileRank the user-specified coordinate
	 * @return true if the coordinate is valid, false otherwise
	 */
	private boolean isValidCoordinate(String fileRank) {
		if (!validRank.contains(fileRank.substring(0, 1)) || 
				!validFile.contains(fileRank.substring(1, 2))) {
			return false;
		}
		return true;
	}
	
	/**
	 * Checks if a move and piece are asking for promotion
	 * @param piece the piece being checked for promotion
	 * @param move the user specified move for promotion
	 * @return true if the move is for promotion, false otherwise
	 */
	private boolean isPromotion(Piece piece, String move) {
		String[] coordinates = move.split(" ");
		String originSpace = coordinates[0];
		String destinationSpace = coordinates[1];
		
		if (originSpace.charAt(1) == '7' && destinationSpace.charAt(1) == '8' && piece.isWhite() || originSpace.charAt(1) == '2' && destinationSpace.charAt(1) == '1' && !piece.isWhite()) {
			if (coordinates.length == 3) { 
				if (!validPromotion.contains(coordinates[2])) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Method to determine whether a promotion move is valid
	 * @param piece the piece being promoted
	 * @param move the user-specified move
	 * @return  true if the move is invalid, false otherwise
	 */
	private boolean isInvalidPromotion(Piece piece, String move) {
		String[] coordinates = move.split(" ");
		String originSpace = coordinates[0];
		String destinationSpace = coordinates[1];
		
		if (originSpace.charAt(1) == '7' && destinationSpace.charAt(1) == '8' && piece.isWhite() || originSpace.charAt(1) == '2' && destinationSpace.charAt(1) == '1' && !piece.isWhite()) {
			if (coordinates.length == 3) { 
				if (coordinates[2].equals("draw?")) {
					return false;
				}
				if (!validPromotion.contains(coordinates[2])) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Method to move a piece according to the user-specified move
	 * @param move the user-specified move
	 * @return GameState enum that provides information about the current state of the game
	 */
	private GameState movePiece(String move) {
		
		String[] coordinates = move.split(" ");
		
		String originSpace = coordinates[0];
		int originFile = originSpace.charAt(0) - 97;
		int originRank = originSpace.charAt(1) - 49;
		Piece originPiece = board[originFile][originRank].getPiece();
		
		String destinationSpace = coordinates[1];
		int destinationFile = destinationSpace.charAt(0) - 97;
		int destinationRank = destinationSpace.charAt(1) - 49;
		
		Piece temp = board[originFile][originRank].getPiece();
		
		if (isPromotion(originPiece, move)) {
			if (isWhiteMove) {
				if (coordinates.length == 3) {
					String promotionPiece = coordinates[2];
					switch (promotionPiece) {
						case ("N"):  board[destinationFile][destinationRank].setPiece(new Knight(destinationFile, destinationRank, "White"));
									 break;
						case ("B"):  board[destinationFile][destinationRank].setPiece(new Bishop(destinationFile, destinationRank, "White"));
									 break;
						case ("R"):  board[destinationFile][destinationRank].setPiece(new Rook(destinationFile, destinationRank, "White"));
									 break;
						case ("Q"):  board[destinationFile][destinationRank].setPiece(new Queen(destinationFile, destinationRank, "White"));
									 break;
						default:  
					}
				}
				else {
					board[destinationFile][destinationRank].setPiece(new Queen(destinationFile, destinationRank, "White"));
				}
			}
			else {
				if (coordinates.length == 3) {
					String promotionPiece = coordinates[2];
					switch (promotionPiece) {
						case ("N"):  board[destinationFile][destinationRank].setPiece(new Knight(destinationFile, destinationRank, "Black"));
									 break;
						case ("B"):  board[destinationFile][destinationRank].setPiece(new Bishop(destinationFile, destinationRank, "Black"));
									 break;
						case ("R"):  board[destinationFile][destinationRank].setPiece(new Rook(destinationFile, destinationRank, "Black"));
									 break;
						case ("Q"):  board[destinationFile][destinationRank].setPiece(new Queen(destinationFile, destinationRank, "Black"));
									 break;
						default:  
					}
				}
				else {
					board[destinationFile][destinationRank].setPiece(new Queen(destinationFile, destinationRank, "Black"));
				}
			}
			board[originFile][originRank].setPiece(null);
			isWhiteMove = !isWhiteMove;
		} else {
			Piece piece = board[destinationFile][destinationRank].getPiece();
			board[destinationFile][destinationRank].setPiece(temp);
			temp.setFile(destinationFile);
			temp.setRank(destinationRank);
			board[originFile][originRank].setPiece(null);
			if((isWhiteMove && getWhiteKing().isInCheck()) || (!isWhiteMove && getBlackKing().isInCheck())) {
				board[originFile][originRank].setPiece(temp);
				temp.setFile(originFile);
				temp.setRank(originRank);
				board[destinationFile][destinationRank].setPiece(piece);
				return GameState.INVALID_MOVE;
			}
			
			isWhiteMove = !isWhiteMove;
			lastPieceMoved = originPiece;
			previousMove = move;
		}

		if((!isWhiteMove && getBlackKing().isInCheck()) || (isWhiteMove && getWhiteKing().isInCheck())) {
			if(!isWhiteMove && getBlackKing().isInCheckmate()) {
				return GameState.WHITE_CHECKMATE;
			} else if(isWhiteMove && getWhiteKing().isInCheckmate()) {
				return GameState.BLACK_CHECKMATE;
			} else {
				return GameState.CHECK;
			}
		}

//		if(isAStalemate()) {
//			return GameState.STALEMATE;
//		}
		
		return GameState.NEXT_MOVE;
	}
	
	/**
	 * Gets the tile at a designated spot
	 * @param file File of the tile
	 * @param rank Rank of the tile
	 * @return Returns the ChessTile at a given file and rank
	 */
	public static ChessTile tileAt(int file, int rank) {
		return board[file][rank];
	}
	
	/**
	 * Gets the current turn on the board
	 * @return True if it is White's Turn, False Otherwise
	 */
	public static boolean isWhiteTurn() {
		return isWhiteMove;
	}
	
	/**
	 * Finds the White King on the board
	 * @return ChessTile that the White King is on
	 */
	private King getWhiteKing() {
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				if(tileAt(i, j).containsPiece()) {
					if(tileAt(i, j).getPiece().toString().equalsIgnoreCase("wK")) {
						return (King)board[i][j].getPiece();
					}	
				}	
			}
		}
		return null;
	}
	
	/**
	 * Finds the White king on the board
	 * @return ChessTile that the Black King is on
	 */
	private King getBlackKing() {
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				if(tileAt(i, j).containsPiece()) {
					if(tileAt(i, j).getPiece().toString().equalsIgnoreCase("bK")) {
						return (King)board[i][j].getPiece();
					}	
				}	
			}
		}
		return null;
	}
	
	private boolean isAStalemate() {
		if(isWhiteMove) {
			for(int i = 0; i < 8; i++) {
				for(int j = 0; j < 8; j++) {
					if(ChessBoard.tileAt(i, j).containsPiece()) {
						if(ChessBoard.tileAt(i, j).getPiece().getColor().equalsIgnoreCase("White")) {
							for(int x = 0; x < 8; x++) {
								for(int y = 0; y < 8; y++) {
									if(ChessBoard.tileAt(i, j).getPiece().isAllowableMove(x, y)) {
										return false;
									}
								}
							}
						}
					}
				}
			}
		} else {
			for(int i = 0; i < 8; i++) {
				for(int j = 0; j < 8; j++) {
					if(ChessBoard.tileAt(i, j).containsPiece()) {
						if(ChessBoard.tileAt(i, j).getPiece().getColor().equalsIgnoreCase("Black")) {
							for(int x = 0; x < 8; x++) {
								for(int y = 0; y < 8; y++) {
									if(ChessBoard.tileAt(i, j).getPiece().isAllowableMove(x, y)) {
										return false;
									}
								}
							}
						}
					}
				}
			}
		}
		return true;
	}
	
	/**
	 * Displays the board onto the console
	 */
	public void displayBoard() {
		for(int i = 7; i >= 0; i--) {
			for(int j = 0; j < board[0].length; j++) {
				if(board[j][i].containsPiece()) {
					System.out.print(board[j][i].getPiece().toString() + " ");
				} else {
					if(isBlackSquare(i, j)) {
						System.out.print("## ");
					} else {
						System.out.print("   ");
					}
				}
			}
			System.out.println("" + (i + 1));
		}
		
		for (int i = 0; i < 8; i++) {
			char column = 'a';
			System.out.print(" " + (char)(column + i) + " ");
		}
		System.out.println("\n");
	}
	
	/**
	 * Helper method for "displayBoard" that determines if the spot is a black space
	 * @param col Column of the space to be examined
	 * @param row Row of the space to be examined
	 * @return If the space examined is a black square or not.
	 */
	private boolean isBlackSquare(int col, int row) {
		if ((row % 2 == 0 && col % 2 == 0) || (row % 2 == 1 && col % 2 == 1)) {
			return true;
		}
		return false;
	}
	
	public static Piece getLastPieceMoved() {
		return lastPieceMoved;
	}
	
	

}
