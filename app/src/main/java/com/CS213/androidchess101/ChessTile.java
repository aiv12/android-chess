package chess;

import chesspieces.Piece;
import chesspieces.PieceType;

/**
 * Class that represents a single tile on the Chess board. 
 * Each Tile tracks its file, rank, and a piece if there exists one. 
 * 
 * @author David Acevedo
 * @author Alex Labrunda
 */

public class ChessTile {
	
	/**
	 * The file (X position) of the piece
	 */
	private int file;
	
	/**
	 * The rank (Y position) of the piece
	 */
	private int rank;
	
	/**
	 * Piece on the location
	 */
	private Piece piece;
	
	/**
	 * Constructor to initialize a tile with a piece
	 * @param file File of the Tile
	 * @param rank Rank of the Tile
	 * @param piece Piece at the tile
	 */
	public ChessTile(int file, int rank, Piece piece) {
		this.file = file;
		this.rank = rank;
		this.piece = piece;
	}
	
	/**
	 * Alternate Constructor to initialize a tile without a piece
	 * @param file File of the tile
	 * @param rank Rank of the Tile
	 */
	public ChessTile(int file, int rank) {
		this(file, rank, null);
	}

	/**
	 * Gets the piece at the tile
	 * @return Piece at the tile
	 */
	public Piece getPiece() {
		return piece;
	}

	/**
	 * Sets the piece at the tile
	 * @param piece Piece to be set
	 */
	public void setPiece(Piece piece) {
		this.piece = piece;
	}

	/**
	 * Gets the file of the tile
	 * @return File Number of the tile
	 */
	public int getFile() {
		return file;
	}

	/**
	 * Gets the rank of the tile
	 * @return Rank Number of the tile
	 */
	public int getRank() {
		return rank;
	}
	
	/**
	 * Checks if the tile contains a piece
	 * @return Whether or not there is a piece at the tile
	 */
	public boolean containsPiece() {
		if(piece == null) {
			return false;
		}
		return true;
	}
	
	/**
	 * Checks if a piece can be occupied by a black space
	 * @return True if it can be occupied by a Black space, False otherwise
	 */
	public boolean canBeAttackedByBlack() {
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				if(ChessBoard.tileAt(i, j).containsPiece()) {
					if(ChessBoard.tileAt(i, j).getPiece().getColor().equalsIgnoreCase("Black") && ChessBoard.tileAt(i, j).getPiece().isAllowableMove(this.file, this.rank) && ChessBoard.tileAt(i, j).getPiece().getType() != PieceType.KING) {
						//System.out.println("file: " + i + ", " + "rank: " + j);
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Checks if a piece can be occupied by a White space
	 * @return True if it can be occupied by a White space, False otherwise
	 */
	public boolean canBeAttackedByWhite() {
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				if(ChessBoard.tileAt(i, j).containsPiece()) {
					if(ChessBoard.tileAt(i, j).getPiece().getColor().equalsIgnoreCase("White") && ChessBoard.tileAt(i, j).getPiece().isAllowableMove(this.file, this.rank) && ChessBoard.tileAt(i, j).getPiece().getType() != PieceType.KING) {
						//System.out.println("file: " + i + ", " + "rank: " + j);
						return true;
					}
				}
			}
		}
		return false;
	}
	
	
	
	

}
