package chess;

import java.util.Scanner;

/**
 * Class that handles the logic of the game and the turns taken
 * 
 * @author David Acevedo
 * @author Alex Labrunda
 */

public class Chess {
	
	/**
	 * Variable that stores the board
	 */
	private static ChessBoard board;
	
	/**
	 * Variable to allow user input
	 */
	private static Scanner sc;
	
	/**
	 * Variable to keep track if the game is over
	 */
	private static boolean gameOver = false;
	
	/**
	 * Variable to keep track of whose turn it is
	 */
	private static boolean isWhiteTurn = true;
	
	/**
	 * Prompts White to move and handles the move
	 */
	private static void promptWhiteMove(ChessBoard board) {
		String move;
		System.out.print("White's move: ");
		move = sc.nextLine();
		GameState gameState = board.makeMove(move, "white");
		switch (gameState) {
			case CHECK:
				System.out.println("\n");
				board.displayBoard();
				System.out.println(gameState.getMessage());
				return;
			case DRAW:
				System.out.println(gameState.getMessage() + "\n");
				gameOver = true;
				return;
			case INVALID_MOVE:
				System.out.println(gameState.getMessage() + "\n");
				promptWhiteMove(board);
				return;
			case NEXT_MOVE:
				System.out.println();
				board.displayBoard();
				return;
			case WHITE_CHECKMATE:
				System.out.println("\n");
				board.displayBoard();
				System.out.println(gameState.getMessage() + "\n");
				gameOver = true;
				return;
			case WHITE_RESIGN:
				System.out.println(gameState.getMessage() + "\n");
				gameOver = true;
				return;
			case STALEMATE:
				System.out.println(gameState.getMessage() + "\n");
				gameOver = true;
				return;
			default:  
		}
	}
	
	/**
	 * Prompts Black to move and handles the move
	 */
	private static void promptBlackMove(ChessBoard board) {
		String move;
		System.out.print("Black's move: ");
		move = sc.nextLine();
		GameState gameState = board.makeMove(move, "black");
		switch (gameState) {
			case BLACK_CHECKMATE:
				System.out.println("\n");
				board.displayBoard();
				System.out.println(gameState.getMessage() + "\n");
				gameOver = true;
				return;
			case BLACK_RESIGN:
				System.out.println(gameState.getMessage() + "\n");
				gameOver = true;
				return;
			case CHECK:
				System.out.println("\n");
				board.displayBoard();
				System.out.println(gameState.getMessage());
				return;
			case DRAW:
				System.out.println(gameState.getMessage() + "\n");
				gameOver = true;
				return;
			case INVALID_MOVE:
				System.out.println(gameState.getMessage() + "\n");
				promptBlackMove(board);
				return;
			case NEXT_MOVE:
				System.out.println();
				board.displayBoard();
				return;
			case STALEMATE:
				System.out.println(gameState.getMessage() + "\n");
				gameOver = true;
				return;
			default:  
		}
	}

	/**
	 * Starts the game of Chess
	 * @param args Unused
	 */
	public static void main(String[] args) {
		
		//Initialize Chess Board
		board = new ChessBoard();
		
		//Initialize Scanner to read user input
		sc = new Scanner(System.in);
		
		//Displays the board
		board.displayBoard();
		
		//Game loop 
		while(!gameOver) {
			//Displays the board
			//board.displayBoard();
			
			//Handles whose turn it is
			if(isWhiteTurn) {
				promptWhiteMove(board);
			} else {
				promptBlackMove(board);
			}
			
			//Switches the turn
			isWhiteTurn = !isWhiteTurn;
		}
	}

}
