package chess;

/**
 * Enum that lists the different types of moves that can be committed.
 * With each Enum, there is a string that gets printed when a state is returned.
 * 
 * @author David Acevedo
 * @author Alex Labrunda
 */

public enum GameState {
	
	BLACK_CHECKMATE("Checkmate\nBlack wins"),
	BLACK_RESIGN("White wins"),
	CHECK("Check"),
	DRAW("Draw"),
	INVALID_MOVE("Illegal move, try again"),
	NEXT_MOVE(""),
	WHITE_CHECKMATE("Checkmate\nWhite wins"),
	WHITE_RESIGN("Black wins"),
	STALEMATE("Stalemate");
	
	String message;
	
	GameState(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return this.message;
	}

}
